I’m a Registered Psychologist with the College of Psychologists of Ontario, an Individual Therapist, Couples Counsellor & Marriage Therapist, a Published Author and a guest Relationship Expert on radio & television with a counselling, psychology and psychotherapy private practice in midtown Toronto.

Address: 2300 Yonge St, #1600, Toronto, ON M4P 1E4, Canada

Phone: 416-549-5089

Website: https://www.susanblackburn.com
